/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    DEBUG.h

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef _DEBUG_H    /* Guard against multiple inclusion */
#define _DEBUG_H


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

/* This section lists the other files that are included in this file.
 */

/* TODO:  Include other files here if needed. */
#include "peripheral/ports/plib_ports.h"
#include "driver/usart/drv_usart.h"



/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif
#define EVENT_TYPE                  unsigned char
#define EVENT_FAIL                  0xFF
#define EVENT_SUCCESS               0x1
#define CREATE_MSG_QUEUE_SUCCESS    0x50 
#define CREATE_WHEEL_MOTOR_TIMER_SUCCESS 0x51
#define WRITING_VALUES                   0x33
    /*****************************************************************************
     * TASK EVENT NUMBERS
     *****************************************************************************/
#define TASK_BEGIN                      0x60
#define WAITING_TO_RECEIVE_FROM_QUEUE   0x61
#define RECEIVED_FROM_QUEUE             0x62
#define SENT_TO_QUEUE                   0x63
#define DISPLAY_ON_GPIO                 0x6A
#define DISPLAY_ON_UART                 0x6B

    /*****************************************************************************
     * ISR EVENT NUMBERS
     *****************************************************************************/
#define ENTER_ISR              0x70
#define ISR_SENT_TO_QUEUE      0x71
#define LEAVING_ISR            0x72

    void dbgOutputVal(unsigned int outVal);
    /***************************************************************************
     * Debug UART Value
     * Value Written to UART
     * Verify that the value of outputVal is less than or equal to 127
     ***************************************************************************/
    void dbgUARTVal(unsigned char outVal, unsigned char centi, unsigned char meter);
    /***************************************************************************
     * STOP EVERYTHING ROUTINE
     * This routine makes sure everything stops working 
     ***************************************************************************/
    void stopEverything();
    void dbgOutputEvent(unsigned char outVal);
    void eventGPIODisplay(unsigned char outVal);
    /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* _EXAMPLE_FILE_NAME_H */

/* *****************************************************************************
 End of File
 */
