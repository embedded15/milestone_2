/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    filename.h

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef _SOFTWARE_TIMER_H    /* Guard against multiple inclusion */
#define _SOFTWARE_TIMER_H


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

/* This section lists the other files that are included in this file.
 */
#include "FreeRTOS.h"
#include "timers.h"
#include "debug.h"
#include "motor_queue.h"
/* TODO:  Include other files here if needed. */


/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif
/* Sets the period to 50 ms */  
    /* Wheel Motor */
     TimerHandle_t wheelMotor_timer;
    /*******************************************************************************
    * 
    *******************************************************************************/
    EVENT_TYPE createWheelMotorTimer(unsigned int period);
    /*******************************************************************************
    * 
    *******************************************************************************/
    EVENT_TYPE startWheelMotorTimer();

    /******************************************************************************/
    /******************************************************************************/
    /*******************************************************************************
    * Internal CallBack Function 
    *******************************************************************************/
    static void readWheelEncoderValues(TimerHandle_t motor_timer);
    
#ifdef __cplusplus
}
#endif

#endif /* _EXAMPLE_FILE_NAME_H */

/* *****************************************************************************
 End of File
 */
