#include "software_timer.h"

/*******************************************************************************
 * 
 *******************************************************************************/
EVENT_TYPE createWheelMotorTimer(unsigned int period) {
    wheelMotor_timer = xTimerCreate("Wheel Motor Timer", 
            pdMS_TO_TICKS(period), 
            pdTRUE,             /* Auto Reset Timer */ 
            0, 
            readWheelEncoderValues);
    if(wheelMotor_timer == NULL) {
        return EVENT_FAIL;
    } 
    return EVENT_SUCCESS;
}
/*******************************************************************************
 * 
 *******************************************************************************/
EVENT_TYPE startWheelMotorTimer() {
    if(xTimerStart(wheelMotor_timer, 0) != pdPASS) {
        return EVENT_FAIL;
    }
    return EVENT_SUCCESS;
}
/*******************************************************************************
* 
*******************************************************************************/

static void readWheelEncoderValues(TimerHandle_t wheel_timer) {
    TickType_t curTime;
    curTime =  xTaskGetTickCount(); 
    //int before = uxQueueSpacesAvailable( motor_queue );
    /* Read Encoder Values */
    // ENCODER VALUES ARE 
    // RG0 for SA1 Encoder
    // RA7 for SB2 Encoder
//    unsigned int RightMotorEncoderValue = PLIB_PORTS_PinGet(PORTS_ID_0, PORT_CHANNEL_G, PORTS_BIT_POS_0); /* 12 / revolution */
//    unsigned int LeftMotorEncoderValue  = PLIB_PORTS_PinGet(PORTS_ID_0, PORT_CHANNEL_A, PORTS_BIT_POS_7); /* 12 / revolution */
//    
    unsigned int motor_speed_left  = DRV_TMR1_CounterValueGet(); /* Timer 1's counter value is driven by the rising edge of SA1 Encoder ticks */
    unsigned int motor_speed_right = DRV_TMR2_CounterValueGet(); /* Timer 2's counter value is driven by the rising edge of SA2 Encoder ticks */
    motor_speed_left >>= 2;
    motor_speed_right >>=2;
    /* Send Message to Message Queue */
    EVENT_TYPE event;
    event = sendMotorMsgToQueue(&motor_speed_left);
    dbgOutputEvent(event);
    event = sendMotorMsgToQueue(&motor_speed_right);
    //int after = uxQueueSpacesAvailable( motor_queue );
    /* Reset the timer counter */
    dbgOutputEvent(event);
    dbgOutputEvent(SENT_TO_QUEUE);
    DRV_TMR1_CounterClear();
    DRV_TMR2_CounterClear();
}
