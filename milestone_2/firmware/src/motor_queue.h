/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    motor_queue.h

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef _MOTOR_QUEUE_H    /* Guard against multiple inclusion */
#define _MOTOR_QUEUE_H


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */
#include "debug.h"
#include "FreeRTOS.h"
#include "queue.h"
/* This section lists the other files that are included in this file.
 */

/* TODO:  Include other files here if needed. */


/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif

    #define MSG_TYPE uint32_t
    #define MSG_LENGTH 10

    QueueHandle_t motor_queue;
    
    
    EVENT_TYPE createMotorMsgQueue();
    EVENT_TYPE sendMotorMsgToQueue(void *message);
    EVENT_TYPE ISRSendMotorMsgToQueue(void * message, portBASE_TYPE* pxHigherPriorityTaskWoken);
    EVENT_TYPE threadReceiveMsgFromMotorQueue(void * message);
    
    

    /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* _EXAMPLE_FILE_NAME_H */

/* *****************************************************************************
 End of File
 */
