#include "debug.h"

/*****************************************************************************
 * 
 *****************************************************************************/
void EventGPIODisplay(unsigned char outVal) {
      
    PLIB_PORTS_Write(PORTS_ID_0, PORT_CHANNEL_E, outVal);
}
/*****************************************************************************
 * 
 *****************************************************************************/
void dbgOutputEvent(unsigned char outVal) {
    // outVal corresponds to an event
    // outVal is a unique value for a specific event
    if (outVal == EVENT_FAIL) {stopEverything();}
    if(outVal <= 127) {
        // Output to a different 8 I/O pins
        // Pins need to be accessible when used with other boards
        //DBG_EVENT_OUT(outVal);
        EventGPIODisplay(outVal);
    }
}
/*****************************************************************************
 * 
 *****************************************************************************/
void dbgOutputVal(unsigned int outVal) {
    // outVal corresponds to an event
    // outVal is a unique value for a specific event
    
        // Output to a different 8 I/O pins
        // Pins need to be accessible when used with other boards
        EventGPIODisplay((outVal & 0xFF00) >> 8);
        EventGPIODisplay(outVal);
}
/*****************************************************************************
 * 
 *****************************************************************************/
void dbgUARTVal(unsigned char outVal, unsigned char centi, unsigned char meter) {
    // Output the average of every four sensor messages
        while(PLIB_USART_TransmitterBufferIsFull(USART_ID_4));
    /* Send one byte */
    PLIB_USART_TransmitterByteSend(USART_ID_4, outVal);
        while(PLIB_USART_TransmitterBufferIsFull(USART_ID_4));
    /* Send one byte */
    PLIB_USART_TransmitterByteSend(USART_ID_4, centi);
        while(PLIB_USART_TransmitterBufferIsFull(USART_ID_4));
    /* Send one byte */
    PLIB_USART_TransmitterByteSend(USART_ID_4, meter);
    
}
/*****************************************************************************
 * 
 *****************************************************************************/
void stopEverything() {
    // Stop everything from running and end the program
    /************************************************************************
     * Disable Interrupts
     ************************************************************************/
    SYS_INT_StatusGetAndDisable();
    //DRV_ADC_Stop();
    
    /************************************************************************
     * Disable Suspend All Threads
     ************************************************************************/
    while(1) {
        //DBG_EVENT_OUT(EVENT_FAIL);
        EventGPIODisplay(EVENT_FAIL);
        vTaskSuspend( NULL );
    }
     
    // Enter infinite while loop 
    // Then send out that the event failed

}
