/*******************************************************************************
  MPLAB Harmony Application Source File
  
  Company:
    Microchip Technology Inc.
  
  File Name:
    motor.c

  Summary:
    This file contains the source code for the MPLAB Harmony application.

  Description:
    This file contains the source code for the MPLAB Harmony application.  It 
    implements the logic of the application's state machine and it may call 
    API routines of other MPLAB Harmony modules in the system, such as drivers,
    system services, and middleware.  However, it does not call any of the
    system interfaces (such as the "Initialize" and "Tasks" functions) of any of
    the modules in the system or make any assumptions about when those functions
    are called.  That is the responsibility of the configuration-specific system
    files.
 *******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END


// *****************************************************************************
// *****************************************************************************
// Section: Included Files 
// *****************************************************************************
// *****************************************************************************

#include "motor.h"

// *****************************************************************************
// *****************************************************************************
// Section: Global Data Definitions
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
/* Application Data

  Summary:
    Holds application data

  Description:
    This structure holds the application's data.

  Remarks:
    This structure should be initialized by the APP_Initialize function.
    
    Application strings and buffers are be defined outside this structure.
*/

MOTOR_DATA motorData;

// *****************************************************************************
// *****************************************************************************
// Section: Application Callback Functions
// *****************************************************************************
// *****************************************************************************

/* TODO:  Add any necessary callback functions.
*/

// *****************************************************************************
// *****************************************************************************
// Section: Application Local Functions
// *****************************************************************************
// *****************************************************************************


/* TODO:  Add any necessary local functions.
*/


// *****************************************************************************
// *****************************************************************************
// Section: Application Initialization and State Machine Functions
// *****************************************************************************
// *****************************************************************************

/*******************************************************************************
  Function:
    void MOTOR_Initialize ( void )

  Remarks:
    See prototype in motor.h.
 */

void MOTOR_Initialize ( void )
{
    // Initialize Timer Drivers
    DRV_TMR0_Start(); /* Timer for OC compare */
    DRV_TMR1_Start(); /* Timer for Encoder SA1 Reading */
    DRV_TMR2_Start(); /* Timer for Encoder SA2 Reading */
    
    // Initialize Output Compares
    DRV_OC0_Start();
    DRV_OC1_Start();
    DRV_OC0_Enable();
    DRV_OC1_Enable();
    // Setting up the pulse width
    DRV_OC0_PulseWidthSet(0x00);
    DRV_OC1_PulseWidthSet(0x00);
    // Initialize the motor Queue 
    EVENT_TYPE events;
    events = createMotorQueue();
    dbgOutputEvent(events);
    dbgOutputEvent(CREATE_MSG_QUEUE_SUCCESS);
    // Initialize the timer with desired timer period in milliseconds
    events = createWheelMotorTimer(1000);
    dbgOutputEvent(events);
    dbgOutputEvent(CREATE_WHEEL_MOTOR_TIMER_SUCCESS);
    events = startWheelMotorTimer();
    dbgOutputEvent(events);
    leftMotor_control(48, FORWARD);
    rightMotor_control(50, FORWARD);
    
}


/******************************************************************************
  Function:
    void MOTOR_Tasks ( void )

  Remarks:
    See prototype in motor.h.
 */


void MOTOR_Tasks ( void )
{
    unsigned int leftSpeed, rightSpeed;
    EVENT_TYPE event;
    dbgOutputEvent(WAITING_TO_RECEIVE_FROM_QUEUE);
    event = threadReceiveMsgFromMotorQueue(&leftSpeed);
    dbgOutputEvent(event);
    event = threadReceiveMsgFromMotorQueue(&rightSpeed);
    dbgOutputEvent(event);
    dbgOutputEvent(RECEIVED_FROM_QUEUE);
    dbgOutputEvent(WRITING_VALUES);
    dbgOutputVal(leftSpeed);
    dbgOutputVal(rightSpeed);
//    while (1) {
//        leftMotor_control(0, 0);
//        rightMotor_control(0, 0);
//    }
}

void leftMotor_control(unsigned int speed, DIRECTION dir) {
    pwmVal = speedConversion(speed);
    DRV_OC0_PulseWidthSet(pwmVal);
    PLIB_PORTS_PinWrite(PORTS_ID_0, PORT_CHANNEL_C, PORTS_BIT_POS_14, dir);
}
void rightMotor_control(unsigned int speed, DIRECTION dir) {
    pwmVal = speedConversion(speed);
    DRV_OC1_PulseWidthSet(pwmVal);
    PLIB_PORTS_PinWrite(PORTS_ID_0, PORT_CHANNEL_G, PORTS_BIT_POS_1, dir);
}

unsigned int speedConversion(unsigned int speed) {
    return (156 * speed) + 25; /* Linear Relationship between 0 - 100 and 0 - 15625 */
}
/*******************************************************************************
 End of File
 */

