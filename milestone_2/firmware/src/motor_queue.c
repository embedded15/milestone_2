#include "motor_queue.h"
/*******************************************************************************************
 * 
 *******************************************************************************************/
EVENT_TYPE createMotorQueue() {
    motor_queue = xQueueCreate(MSG_LENGTH, sizeof(MSG_TYPE));
    if(motor_queue == NULL) {
        return EVENT_FAIL;
    }
    return EVENT_SUCCESS;
}
/*******************************************************************************************
 * 
 *******************************************************************************************/
EVENT_TYPE sendMotorMsgToQueue(void *message) {
    if(xQueueSendToBack(motor_queue, message, 0) != pdPASS) {
        return EVENT_FAIL;
    }
    return EVENT_SUCCESS;
}
/*******************************************************************************************
 * 
 *******************************************************************************************/
EVENT_TYPE ISRSendMsgToMotorQueue(void * message, portBASE_TYPE* pxHigherPriorityTaskWoken) {
    if(xQueueSendToBackFromISR(motor_queue, message, pxHigherPriorityTaskWoken) != pdPASS) {
        return EVENT_FAIL;
    }
    return EVENT_SUCCESS;
}
/*******************************************************************************************
 * 
 *******************************************************************************************/
EVENT_TYPE threadReceiveMsgFromMotorQueue(void * message) {
    if (xQueueReceive(motor_queue, message, portMAX_DELAY) != pdPASS) {
        // Hand Error
        return EVENT_FAIL;
    }
    return EVENT_SUCCESS;
}
