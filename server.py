#!/usr/bin/python3

# ECE4534 Team15 server
"""
This server will receive HTTP requests and provide 
HTTP responses with JSON payloads.
"""

import socket
from http.server import BaseHTTPRequestHandler, HTTPServer
from io import StringIO
import json
import requests
import pymongo
from pymongo import MongoClient
import time

host_ip = "127.0.0.1"
port = 2000
backlog = 5
size = 1024

# creating database
# client = MongoClient(host_ip, port)
# db = client.test_database

# class to handle incoming requests
class TeamServer(BaseHTTPRequestHandler):

	# retrieve information from client
	def do_GET(self):
		# Send response status code
		self.send_response(200)
		# Send headers
		self.send_header('Content-type','application/json')
		self.end_headers()
		# Send message back to client
		message = ["Request received"]
		# Write content as utf-8 data
		self.wfile.write(bytes(json.dumps(message), "utf8"))

		return

	# send data to the server
	def do_POST(self):
		# Send response status code
		self.send_response(200)
		# create and send to database
		# posts = db.posts
		# post_id = posts.insert_one(post).inserted_id
		# Send message back to client
		message = ["Data sent to server"]
		# Write content as utf-8 data
		self.wfile.write(bytes(json.dumps(message), "utf8"))
		return

try:	
	# server and handler to manage incoming requests
	teamServer = HTTPServer((host_ip, port), TeamServer)
	print (time.asctime(), "Server opened on %s:%s" % (host_ip, port))

	# wait forever for incoming requests
	teamServer.serve_forever()

except KeyboardInterrupt:
	print(time.asctime(), "Server closed on %s:%s" % (host_ip, port))
	teamServer.server_close()
